### Liste des ministères et administrations engagés dans le Plan d'action national 2015-2017


- Premier ministre
    * Secrétariat d’Etat chargé de la Réforme de l’Etat et de la Simplification
    * Direction de l’information légale et administrative
    * Agence nationale de la sécurité des systèmes d'information
- Ministère des Affaires étrangères et du Développement international
- Ministère de l’Environnement, de l’Energie et de la Mer
- Ministère de l’Education nationale, de l’Enseignement supérieur et de la Recherche
- Ministère de la Justice
- Ministère de l’Aménagement du territoire, de la Ruralité et des Collectivités territoriales
- Ministère de l’Intérieur
- Ministère des Finances et des Comptes publics
- Ministère de l'Economie, de l'Industrie et du Numérique
    * Secrétariat d’Etat chargé du Numérique
- Ministère de la Ville, de la Jeunesse et des Sports
- Ministère de la Fonction publique

Portent également des engagements : 
- Cour des comptes
- Haute Autorité pour la transparence de la vie publique