# Organisez un événement "Ministère ouvert" sur vos engagements !

Les ministères porteurs d'engagements peuvent organiser leurs propres événements contributifs "Ministère ouvert" dans le cadre du suivi de la mise en œuvre du Plan d'action national de la France, avec le soutien de la mission Etalab du Secrétariat général pour la modernisation de l’action publique.

## 1. Qu'est ce qu'un événement "Ministère ouvert" ? 

Pour évaluer la mise en oeuvre de son Plan d'action national, la France a mis en place un dispositif de consultation en ligne, via ce livre électronique,
et physique, via les événements "Ministère ouvert". 

Le dispositif "Ministère ouvert" est un format inédit de dialogue et de co-construction de l'action publique, qui rassemble la société civile 
et l'administration lors d'ateliers contributifs dans les ministères porteurs d'engagements. 

Un premier événement "Ministère ouvert" a eu le 21 juin 2016 au secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification, porteur de dix
engagements. De 17h à 20h, répartis en ateliers, les participants ont pu échanger avec les agents de l'administration puis présenter leurs contributions au secrétaire d'Etat. 

C'est à partir de l'expérience positive de ce premier événement que le présent kit "Ministère ouvert" a été développé, afin que tous les ministères qui
le souhaitent puissent s'engager dans cette démarche.

[Voir le programme "Ministère ouvert" au Secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification](https://www.etalab.gouv.fr/wp-content/uploads/2016/06/20160621_Programme-Ministere-ouvert-1-VF.pdf).

Les événements "Ministère ouvert" peuvent être organisés tout au long de l'année, jusqu'à la remise du rapport d'autoévaluation final en juillet 2017. 

## 2. Comment organiser un événement "Ministère ouvert" ?

Si vous souhaitez organiser un événement "Ministère ouvert", vous pouvez nous écrire à gouvernement-ouvert@etalab.gouv.fr (ou écrire à votre 
interlocuteur habituel au sein de la mission Etalab). Nous pourrons ainsi discuter plus en détail de votre projet. 

Sur cette page, nous mettons à disposition toutes les ressources méthodologies et visuelles dont vous aurez besoin dans la mise en place de votre
événement "Ministère ouvert". 

* **Le format type des ateliers** > [Ministère ouvert - format type atelier](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Ministère-ouvert-format-type-atelier.odt)
* **Le modèle pour décrire le programme de l'événement** > [Ministère ouvert - Modèle programme](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Ministère-ouvert-Modèle-programme.odt) 
* **Le support de présentation utilisé en salle plénière** > [Ministère ouvert - Modèle présentation plénière](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Ministere-ouvert-Modèle-présentation-plénière.odp)
* **Les supports pour les ateliers** > [Ministère ouvert - Supports ateliers](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Ministère-ouvert-Modèle-supports-ateliers.odp)

## 3. Comment promouvoir votre événement "Ministère ouvert" dans le cadre du suivi du Plan d'action national ? 

Etalab relayera votre événement "Ministère ouvert" par l'intermédiaire de ses canaux de communication et peut également mobiliser ses écosystèmes.
Les restitutions des ateliers peuvent être directement intégrées comme contributions au suivi du plan d'action, à l'instar des [restitutions du Ministère ouvert #1](https://forum.etalab.gouv.fr/users/MinistereOuvert1).

* **Logo "Ministère ouvert"** > [télécharger ici](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Logo_Ministere_Ouvert_V17062016.png)

Contact : <gouvernement-ouvert@etalab.gouv.fr>
