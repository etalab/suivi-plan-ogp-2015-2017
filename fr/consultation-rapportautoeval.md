# 2.3. Consultation sur le rapport d'autoévaluation

## Le rapport d'autoévaluation à un an a été soumis à [consultation publique du 13 au 30 juin 2016](https://www.etalab.gouv.fr/pgo-consultation-sur-le-rapport-detape-du-plan-daction-national-2015-2017)

Cette consultation a été réalisée en ligne et hors ligne. 
- En ligne par l'intermédiaire du forum de discussion ouvert
- En présentiel par l'organisation d'un [Ministère ouvert le 21 juin 2016](https://www.etalab.gouv.fr/plan-daction-pgo-un-premier-evenement-ministere-ouvert-le-21-juin-2016) au Secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification.

## Un processus de dialogue et d'amélioration continue

Afin de pérenniser la démarche consultative instaurée depuis l'élaboration du Plan d'action national, le forum de discussion est ouvert en continu. Des rencontres "Ministère ouvert" pourront être organisées dans les ministères porteurs afin de continuer ce dialogue administrations-société civile sur les engagements du Plan d'action 2015-2017 et sur les futurs engagements à porter dans le prochain Plan d'action.


