# Engagement 7 : Identifier les bénéficiaires effectifs des entités juridiques enregistrées en France pour lutter efficacement contre le blanchiment


## Institutions porteuses : 
- Ministère de la Justice
- Ministère des Finances et des Comptes publics
- Ministère de l'Économie, de l'Industrie et du Numérique


## Enjeux : 
**La connaissance des clients et bénéficiaires des activités financières est l'un des piliers de la
lutte anti-blanchiment, de la lutte contre la corruption et de l'évasion fiscale** et permet de
**déceler des opérations atypiques pouvant être liées à des transactions délictueuses.**

Le bénéficiaire effectifs d'une société, tel que défini à l'article L561-2-2 du Code monétaire et
Financier est « _la personne physique qui contrôle, directement ou indirectement, le client ou
celle pour laquelle une transaction est exécutée ou une activité réalisée_ ». Son identification,
prévue en particulier à l'article L561- 5 du même code, permet de renforcer la transparence
globale des sociétés-écran et des trusts, et de lutter contre le blanchiment de capitaux, la
corruption et l'évasion fiscale.


## Description de l'engagement : 
**Utiliser un registre centralisé, abondé de données variées, incluant les données du
registre français centralisé pour les entreprises (le Registre du Commerce et des Sociétés,
RCS), de manière à assurer et à fournir un accès largement ouvert à des informations
utiles, exactes et à jour sur les bénéficiaires effectifs des sociétés et autres entités
juridiques.**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-vie-economique/index.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Utiliser un registre centralisé, abondé de données variées, incluant les données du registre français centralisé pour les entreprises (le Registre du Commerce et des Sociétés, RCS). | A l'occasion du Sommet Anti-Corruption tenu à Londres le 12 mai 2016, la France a pris plusieurs engagements sur les registres des bénéficiaires effectifs ([lien vers les engagements français](https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/522751/France.pdf)). Parallèlement, [la loi n. 2016-1691 relative à la transparence, à la lutte contre la corruption et à la modernisation de la vie économique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033558528&categorieLien=id) adoptée le 9 décembre 2016 instaure dans son article 139 une section dans le code monétaire et financier relative au bénéficiaire effectif. Les sociétés doivent désormais communiquer au registre du commerce et des sociétés les informations sur leurs bénéficiaires effectifs. Des décrets prévus en avril 2017 définieront les modalités de collectes des informations ainsi que d'accès et de mise à disposition du public ([voir l'échéancier des décrets](https://www.legifrance.gouv.fr/affichLoiPubliee.do;jsessionid=C0DB307501623A4C6EFCC684162F0D5C.tpdila13v_3?idDocument=JORFDOLE000032319792&type=echeancier&typeLoi=&legislature=14)). | Suivre les travaux sur les décrets d'application de la loi transparence et définir de manière plus détaillée : 1/ les modalités de collecte des informations. 2/ les sanctions ou dissuasions éventuelles pour assurer la fiabilité des informations des entreprises. 3/ les modalités d'accès au registre du commerce et des sociétés enrichi. L'objectif étant de l'ouvrir largement, un travail avec les greffes et Etalab serait opportun afin d'évaluer les besoins techniques d'ouverture en open data ainsi que les nécessaires ajustements à opérer pour respecter la législation en vigueur (respect de la vie privée). Des accès sécurisés peuvent être envisagés pour certains réutilisateurs. Suivre les modalités d'accès au registre des propriétaires des trusts, [dont la publicité a été suspendue en juillet 2016](http://www.conseil-etat.fr/Actualites/Communiques/Registre-public-des-trusts) (voir [décret initial du 10 mai 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032511026&categorieLien=id). Enfin, la France a réitéré ces engagements en souscrivant  à [l'action collective n°2 de la Déclaration de Paris "Ending abuse of anonymous companies](https://paris-declaration.ogpsummit.org/topic/5810a95bfade72c82462b343). Elle s'est engagée à participer à la définition de standards internationaux afin d'améliorer l'accès et l'utilisation des données des bénéficiaires effectifs. | Substantiel


### Contributions associées : [#1](https://forum.etalab.gouv.fr/t/engagement-7-identifier-les-beneficiaires-effectifs-des-entites-juridiques-enregistrees-en-france-pour-lutter-efficacement-contre-le-blanchiment-rapport-dautoevaluation-a-mi-parcours-du-plan-daction-pour-la-france-2015-2017-pour-une-action-publique-transparente-et-collaborative/1908/2)