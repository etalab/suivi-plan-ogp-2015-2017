# Engagement 11 : Coproduire avec la société civile les registres-clés de données essentielles à la société et à l'économie

## Institutions porteuses
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification
- Secrétariat d'État chargé du Numérique

## Enjeux : 
**De nouvelles formes de coopérations entre les autorités publiques et les citoyens permettent désormais de créer de nouveaux biens communs**, indispensables au service public, à la société et à l'économie, d'une manière plus rapide, plus efficace et moins coûteuse que par le passé.

## Description de l'engagement : 

**Multiplier les coopérations entre acteurs publics et société civile pour la constitution d'infrastructures de données essentielles et de registres-clés de données.**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/consulter-concerter-co-produire/action-publique/engagement-11.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Multiplier les coopérations entre acteurs publics et société civile pour la constitution d’infrastructures de données essentielles et de registres-clé de données. | Le service public de la donnée créé par l’article 14 de la [loi pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=2B1AB79A526C62E128619EEEED095E24.tpdila08v_3?cidTexte=JORFTEXT000033202746&categorieLien=id) vise à mettre à disposition, en vue de faciliter leur réutilisation, les jeux de données de référence qui présentent le plus fort impact économique et social. Il s’adresse principalement aux entreprises et aux administrations pour qui la disponibilité d’une donnée de qualité est critique. Les producteurs et les diffuseurs prennent des engagements auprès de ces utilisateurs. En amont de la publication du [décret d'application](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=2B1AB79A526C62E128619EEEED095E24.tpdila08v_3?cidTexte=JORFTEXT000034194946&dateTexte=)en mars 2017, la mission Etalab a mené une consultation publique durant l'automne 2016 ([consulter la synthèse](http://www.etalab.gouv.fr/consultation-spd)) afin d'identifier les bases de données de références et de prédéfinir des critères de qualité de mise à disposition. A ce jour, les neuf jeux de données de référence, disponible sur [une page spécifique sur data.gouv](http://www.data.gouv.fr/fr/reference) sont les suivants : la Base Adresse Nationale, la base SIRENE, le Code Officiel Géographique, le Plan Cadastral Informatisé, le Registre Parcellaire Graphique, le Référentiel de l'organisation administrative de l'Etat, le Référentiel à grande échelle, le Répertoire national des associations, le Répertoire opérationnel des métiers et des emplois. | Un arrêté du Premier ministre viendra prochainement détailler les niveaux de qualité et de disponibilité attenuds, et chacun des producteurs sera amené à publier en réponse ses engagements de service. | Substantiel voire complet
