# Engagement 8 : Renforcer la transparence des paiements et revenus issus des industries extractives

**Institutions porteuses** :
- Ministère des Affaires étrangères et du Développement international
- Ministère de l'Environnement, de l'Energie et de la Mer
- Ministère des Finances et des Comptes publics
- Ministère de l'Économie, de l'Industrie et du Numérique

### Enjeux

**La transparence sur les industries extractives vise à promouvoir une plus grande
responsabilité sociale des entreprises et une meilleure gouvernance publique**, ainsi qu'à
accroître la confiance des investisseurs et du public dans le secteur minier.

Elle répond également au devoir d'exemplarité que la France souhaite exercer vis-à-vis des
pays en développement et des pays émergents, en renforçant les normes qui contribuent à
mettre les entreprises internationales sur un pied d'égalité. Elle accompagne la volonté
politique de développer une activité minière responsable en Guyane et de promouvoir le
domaine minier métropolitain.

## Description de l'engagement : 
**Adhérer à l'Initiative pour la Transparence dans les Industries extractives (ITIE) et travailler sur l'accessibilité des données ouvertes dans le cadre de l'ITIE et des déclarations des entreprises au titre du chapitre 10 de la directive comptable européenne :**
* Été 2015 : désignation du haut représentant français pour l'ITIE et mise en place d'une équipe projet dotée des moyens humains et financiers nécessaires pour préparer la candidature de la France à l'ITIE
* Septembre 2015 : constitution d'un comité national tripartite pour l'ITIE
* Mars 2016 : première déclaration des entreprises au titre du chapitre 10 de la directive comptable
* Avant décembre 2016 : présentation de la candidature de la France à l'ITIE
* 1er semestre 2017 : la France devient « pays candidat » de l'ITIE

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-vie-economique/engagement-8.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Adhérer à l’[Initiative pour la Transparence dans les Industries extractives]( https://eiti.org/fr/itie) (ITIE) et travailler sur l’accessibilité des données ouvertes dans le cadre de l’ITIE et des déclarations des entreprises au titre du chapitre 10 de la directive comptable européenne. | Le Président de la République a annoncé que la France entendait rejoindre l’ITIE en juin 2013, à l’occasion du Sommet du G7 de Lough Erne. Les Etats-Unis, le Royaume Uni et l’Allemagne, ont présenté leur candidature au Conseil d’administration de l’ITIE, en 2014, 2015, et 2016 respectivement. Le processus d'adhésion de la France a été officiellement lancé en février 2016 ([communiqué de presse](http://proxy-pubminefi.diffusion.finances.gouv.fr/pub/document/18/20536.pdf)). Un groupe de travail, présidé par Isabelle Wallard (CGIET) a été constitué, afin de préparer l'adhésion et définir les modalités (périmètre financier, géographique, constitution du comité tripartite qui préparera la candidature et coordonnera la mise en œuvre de l’ITIE). Le communiqué de presse indique que la France entend présenter sa candidature à l’ITIE d’ici la fin 2017. Deux réunions inter administration se sont tenues (mars et avril 2016), mobilisant la DGALN, la DGEC, la DGT, la DGFIP, la DGOM, et la DGM, et portant principalement sur la la définition du périmètre financier de l’ITIE. Les entreprises extractives ont été sensibilisées et soutiennent la démarche. Une question se pose quant à l’intégration des territoires d’outre-mer à cette initiative, la Guyane et la Nouvelle Calédonie notamment. Le sujet du secret fiscal, qui devra être partiellement levé pour permettre la publication d’informations financières dans les rapports annuels attendus, fait également l’objet de travaux internes. Une table ronde avec la société civile (ONG de plaidoyer international, syndicats, journalistes, chercheurs) a été organisée le 3 juin 2016. Les participants, qui se sont montrés intéressés par l’initiative, ont indiqué qu’ils ne se sentaient pas suffisamment légitimes pour représenter la société civile au sein du comité tripartite. Selon eux, les ONG et associations françaises travaillant sur les aspects sociaux et environnementaux de l’exploitation minière et pétrolière devaient être pleinement associées pour rendre cet exercice crédible. Les contacts pris par l’administration auprès de ces associations n’ont pas été concluants. Les travaux de préparation de la candidature et de constitution du comité tripartite ont donc été suspendus indéfiniment. Cet engagement ne pourra pas être mené à bien tant que des moyens humains et financiers dédiés pour soutenir l’ITIE ne seront pas alloués. | Le ministre de l’Economie et des Finances a souligné le 9 décembre 2016 lors du 4ème Sommet mondial du Partenariat pour un gouvernement ouvert  que la révision du Code des Mines, actuellement en examen au Parlement, devait permettre l’adhésion de la France à l’ITIE avant la fin 2017. Les contacts avec la société civile, et notamment auprès des ONG et associations travaillant sur la problématique de l’exploitation minière, pétrolière et gazière sur le territoire français, devront être relancés et élargis au plus vite afin de faciliter la constitution d’un collège de la société civile au sein du comité tripartite de l’ITIE-France. Une fois que ce comité sera constitué, un événement de lancement pourra être organisé. L’Etat, les entreprises, et la société civile devront par la suite trouver un accord sur le périmètre financier et géographique de l’ITIE.Des moyens humains et financiers pour faciliter la coordination de cet exercice devront être alloués. | Partiel
Action supplémentaire   | Une réunion a eu lieu entre Etalab et Isabelle Wallard pour initier les réflexions sur l'ouverture des données des entreprises du secteur extractif dans le cadre de l'adhésion à l'ITIE. | Travailler à ouvrir les données contenues dans les rapports annuels des entreprises extractives françaises.| Partiel

### Contributions associées : [1](https://forum.etalab.gouv.fr/t/engagement-8-renforcer-la-transparence-des-paiements-et-revenus-issus-des-industries-extractives-rapport-dautoevaluation-a-mi-parcours-du-plan-daction-pour-la-france-2015-2017-pour-une-action-publique-transparente-et-collaborative/1909/2)