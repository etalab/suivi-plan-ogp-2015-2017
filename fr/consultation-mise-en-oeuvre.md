# 2.2. Consulation lors de la mise en oeuvre du Plan d'action national

La mise en oeuvre concrète du plan d'action national suppose d'ouvrir aux citoyens des outils de suivi et de participation en ligne et hors ligne. 

## Plusieurs dispositifs sont proposés : 

- Suivre l'avancement des actions réalisées engagement par engagement, publié dans le présent rapport. Les citoyens peuvent commenter directement ce suivi par l'intermédiaire d'un forum, intégré au sein de chaque page de suivi.

                                       

- Envoyer une contribution écrite à l'adresse <gouvernement-ouvert@etalab.gouv.fr>. Les contributions seront rendues publiques. 
                    
                    
- Suggérer des améliorations dans la méthodologie de consultation et de suivi du Plan d'action national, dans la section **["Contribuer"](contribuer.md)** de ce présent livre : les contributeurs peuvent suggérer des outils et méthodologies de consultation, ou encore toute autre idée qui permette d'améliorer le suivi des engagements (construction d'indicateurs quantitatifs et qualitatifs, formats d'ateliers présentiels, etc.) 

En parallèle, les administrations sont invitées à rendre compte de l'avancement de leurs actions.

## Calendrier :

![calendrier de suivi](images/calendrierbis.png)