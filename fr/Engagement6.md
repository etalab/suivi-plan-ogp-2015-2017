# Engagement 6 : Faciliter l'accès aux données relatives aux obligations de transparence des responsables publics

## Institution porteuse : 
- Haute Autorité pour la transparence de la vie publique

## Enjeux : 
**Depuis les lois du 11 octobre 2013, la transparence de la vie publique connait une nouvelle impulsion**. Ces lois prévoient notamment que les 10 000 plus hauts responsables publics élus ou nommés doivent déclarer leur patrimoine et leurs intérêts à la **Haute Autorité pour la transparence de la vie publique (HATVP)**, chargée de les contrôler, en associant largement la société civile à ces contrôles.

Les déclarations de situation patrimoniale et d'intérêts des membres du gouvernement ainsi
que les déclarations d'intérêts des parlementaires nationaux et européens et des élus locaux
sont rendues publiques et diffusées sur le site internet de la HATVP. Selon le Conseil
constitutionnel, cette publication permet « à chaque citoyen de s'assurer par lui-même de la
mise en œuvre des garanties de probité et d'intégrité de ces élus, de prévention des conflits
d'intérêts et de lutte contre ceux-ci». Par ailleurs, il est également permis à un citoyen de
porter à la connaissance de la Haute Autorité des informations dont il dispose et qui ne
figureraient pas dans les déclarations publiées.

Les déclarations peuvent être transmises à la HATVP par voie papier, ou depuis le décret du 3
mars 2015, par télé service. Ce nouvel outil de déclaration en ligne permet d'améliorer
l'accessibilité des déclarations publiées, en évitant les problèmes d'interprétations liés à des
déclarations manuscrites. Cette dynamique doit ainsi être poursuivie en encourageant la
diffusion des données publiées dans un format ouvert et aisément exploitable.

## Description de l'engagement : 

- **Publier sous format ouvert et réutilisable les données publiques des déclarations de situation patrimoniale et d'intérêts soumises à publicité et effectuées par l'intermédiaire d'un télé-service** (déclarations de situation patrimoniale des membres du gouvernement et déclarations d'intérêts des membres du gouvernement, parlementaires, représentants français au Parlement européen et principaux élus locaux)
    * L'évolution de l'application pour déclarer en ligne (ADEL) rendra possible, courant 2016, la diffusion des informations dans un format réutilisable
    * L'action de sensibilisation, menée par la HATVP en faveur de la télé-déclaration, sera intensifiée auprès des personnes soumises aux obligations déclaratives, pour garantir un fort taux de télé déclaration et, partant, un volume important de données publiées en open data


[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-responsables-publics/engagement-6.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Publier sous format ouvert et réutilisable les données publiques des déclarations de situation patrimoniale et d’intérêts soumises à publicité et effectuées par l’intermédiaire d’un télé-service. | [Le décret n° 2016-570 du 11 mai 2016](https://www.legifrance.gouv.fr/eli/decret/2016/5/11/PRMX1607948D/jo) relatif à la transmission à la Haute Autorité pour la transparence de la vie publique des déclarations de situation patrimoniale et déclarations d’intérêts par l’intermédiaire d’un téléservice a consacré la généralisation de la télédéclaration : depuis le 15 octobre 2016, tous les responsables publics concernés doivent utiliser le téléservice [ADEL](http://declarations.hatvp.fr/) pour réaliser leurs déclarations de patrimoine et d’intérêts. Disponible depuis le 30 mars 2015, le téléservice ADEL a permis de renforcer l’accessibilité et la lisibilité des déclarations publiées grâce à une présentation standardisée des informations. Il a rencontré un succès rapide puisque son taux d’utilisation a fortement cru depuis le lancement (20% en mars 2015) pour s’établir, au premier trimestre 2016, à environ 60%. La généralisation de la télédéclaration permet à la Haute Autorité de concrétiser son engagement en matière d’ouverture des données puisqu’elle offre les conditions nécessaires à la publication, en open data, des informations contenues dans les déclarations. En outre, la Haute Autorité a mis en service, en novembre 2016, une nouvelle version accessible de son téléservice ADEL Access qui permet aux personnes en situation de handicap ou possédant un matériel informatique ancien de déposer leurs déclarations en ligne. Cette nouvelle version a été labellisée « e-accessible » de niveau 5. |La Haute Autorité sera techniquement prête à partir de la fin du 1er trimestre 2017. Un projet informatique en cours vise effectivement à permettre l’anonymisation de certaines informations contenues dans les déclarations (ex : adresse personnelle, nom du conjoint, localisation des biens immobiliers etc.) selon un processus informatique adapté au choix du format ouvert. Les déclarations reçues à l'issue des élections présidentielle et législatives de 2017 constitueront donc le 1er lot de déclarations publiées en open data. Cela vise les déclarations de patrimoine et d'intérêts des membres du Gouvernement ainsi que les déclarations d'intérêts et d'activités des 577 députés. Les élections sénatoriales qui se tiendront en septembre 2017 donneront également lieu à la publication, en open data, des données des déclarations d'intérêts des 170 sénateurs de la série 1. Au total, ce sont plus de 700 déclarations qui seront publiées en open data au cours de la période 2017-2018. | Complet

## Autres avancées

A l'occasion du Sommet PGO 2016, la France a souscrit à [l'action collective relative à la transparence du lobbying](https://paris-declaration.ogpsummit.org/topic/58136856f224461c2c31c81a) dans laquelle elle rappelle l’établissement, au cours de l’année 2017, d’un registre public unique et obligatoire du lobbying qui sera consultable en ligne et dont la mise en œuvre et le contrôle seront assurés par la Haute Autorité (cf. loi n°2016-1691 relative à la transparence la lutte contre la corruption et la modernisation de la vie économique).

Ce registre permettra une plus grande transparence dans les rapports entre les représentants d’intérêts (entreprises privées, établissements publics exerçant une activité industrielle et commerciale tels que la SNCF ou l’INA, chambres consulaires, lobbyistes professionnels, associations etc.) et les responsables publics (membres du Gouvernement et leurs collaborateurs, parlementaires et leurs collaborateurs, collaborateurs du Président de la République, hauts fonctionnaires, membres des autorités administratives indépendantes disposant d’un pouvoir de sanction, certains élus locaux et fonctionnaires territoriaux).

Les représentants d’intérêts devront obligatoirement rendre publiques, sur ce registre, les informations liées à leurs activités d’influence auprès des responsables publics (intérêts représentés, actions menées, dépenses liées etc.) :
- au commencement de leur activité, les représentants d’intérêts devront communiquer certaines informations générales (identité, champ des activités de représentations d’intérêts, actions de lobbying menées l’année précédant la déclaration et les coûts y afférents, appartenance à des organisations représentatives, nombre de personnes employées pour les actions de lobbying, chiffre d’affaires réalisé l’année précédant la déclaration) afin de permettre leur inscription dans le répertoire ;

- par la suite, les représentants d’intérêts seront tenus de faire état, selon un rythme qui sera définit par décret, du bilan des activités de représentation d’intérêts exercées pendant la période écoulée, en précisant notamment le montant des dépenses et du chiffre d’affaires associés à ces activités.

Dans une logique de transparence, l’ensemble de ces informations seront rendues publiques en open data sur le site internet de la Haute Autorité (www.hatvp.fr) afin de permettre leur réutilisation effective.



## Une belle histoire 

Lors de sa création, la Haute Autorité pour la transparence de la vie publique a été chargée par le législateur de permettre la réutilisation en open data des informations issues des déclarations d'intérêts des élus français. Pour les premières déclarations, reçues au format papier, et faute de temps et de moyens pour les ressaisir, la Haute Autorité a dû publier des images scannées de ces documents, rendant de ce fait impossible l'analyse systématique de ces documents par les citoyens ou les journalistes.

En juillet 2014, l'association Regards Citoyens a donc lancé une initiative de crowdsourcing permettant à n'importe quel citoyen de participer à la conversion en open data des informations déclarées par les parlementaires et les membres du Gouvernement français. À travers une interface très simple, les internautes anonymes étaient invités à ressaisir sous forme numérique les informations manuscrites remplies par les parlementaires et les membres du Gouvernement. Afin d'éviter toute erreur de saisie, les informations n'étaient validées qu'une fois saisies à l'identique par trois internautes différents.

En 10 jours, 8000 personnes ont participé à cette opération et ont permis de rendre public en open data 20 005 informations unitaires relatives aux intérêts des parlementaires et des ministres. Ces données ont été très largement réutilisées notamment par la presse française.

A partir du 15 octobre 2016, la HATVP a annoncé la télédéclaration obligatoire pour les 14 000 personnes entrant dans le champ de la Haute Autorité. Cela lui permettra de publier les informations relatives aux déclarations d'intérêts en open data en 2017.

### Contributions associées : [#1](https://forum.etalab.gouv.fr/t/engagement-6-faciliter-lacces-aux-donnees-relatives-aux-obligations-de-transparence-des-responsables-publics-rapport-dautoevaluation-a-mi-parcours-du-plan-daction-pour-la-france-2015-2017-pour-une-action-publique-transparente-et-collaborative/1907/2), [#2](https://forum.etalab.gouv.fr/t/engagement-6-faciliter-lacces-aux-donnees-relatives-aux-obligations-de-transparence-des-responsables-publics-rapport-dautoevaluation-a-mi-parcours-du-plan-daction-pour-la-france-2015-2017-pour-une-action-publique-transparente-et-collaborative/1907/3)