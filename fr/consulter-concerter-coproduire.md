# Consulter, concerter, co-produire

- [Engagement 10 : Donner aux citoyens de nouveaux moyens de participer à la vie publique en les associant à l'identification de problèmes à résoudre](Engagement10.md)
- [Engagement 11 : Coproduire avec la société civile les registres-clés de données essentielles à la société et à l'économie](Engagement11.md)
- [Engagement 12 : Poursuivre l'ouverture des ressources juridiques et la collaboration avec la société civile autour de l'élaboration de la loi](Engagement12.md)
- [Engagement 13 : Capitaliser sur les consultations menées et rénover les dispositifs d'expression citoyenne](Engagement13.md)
- [Engagement 14 : Renforcer la médiation et la capacité d'agir des citoyens en matière de justice](Engagement14.md)