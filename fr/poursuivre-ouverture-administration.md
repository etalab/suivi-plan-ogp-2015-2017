# Poursuivre l'ouverture de l'administration

- [Engagement 19 : Permettre l'engagement de la société civile en appui de l'École](Engagement19.md)
- [Engagement 20 : Diversifier le recrutement au sein des institutions publiques](Engagement20.md)
- [Engagement 21 : Diffuser la culture de l'ouverture, des données et du numérique](Engagement21.md)
- [Engagement 22 : Diffuser l'innovation et approfondir la recherche sur le gouvernement ouvert](Engagement22.md)
- [Engagement 23 : Responsabiliser et protéger les agents publics en matière de prévention des conflits d'intérêts](Engagement23.md)