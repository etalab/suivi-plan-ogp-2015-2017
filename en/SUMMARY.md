# Mid-term self-assessment report of France’s 2015-2017 National Action Plan “For a transparent and collaborative public action”

* [About this ebook](/en/README.md)
* [Introduction](/en/Introduction.md)
* [Process of developing and implementing the Nation Action Plan](/en/consultation.md)
  * [Consultation on drafting the National Action Plan](/en/consultation-drafting.md)
  * [Consultation during the National Action Plan's implementation](/en/consultation-implementing.md)
  * [Consultation on the self-assessment report](/en/consultation-report.md)
* [Recommendations of the Independent Reporting Mechanism](/en/irm-recommendations.md)

* [Implementation of the National Action Plan commitment](/en/implementation-commitments.md)

  * [Summary table](/en/summary-table.md)
  * [Results obtained for each commitment](/en/details-commitments.md)
    * [Commitment 1](/en/commitment 1.md)
    * [Commitment 2](/en/commitment 2.md)
    * [Commitment 3](/en/commitment 3.md)
    * [Commitment 4](/en/commitment 4.md)
    * [Commitment 5](/en/commitment 5.md)
    * [Commitment 6](/en/commitment 6.md)
    * [Commitment 7](/en/commiment 7.md)
    * [Commitment 8](/en/commitment 8.md)
    * [Commitment 9](/en/commitment 9.md)
    * [Commitment 10](/en/commitment 10.md)
    * [Commitment 11](/en/commitment 11.md)
    * [Commitment 12](/en/commitment 12.md)
    * [Commitment 13](/en/commitment 13.md)
    * [Commitment 14](/en/commitment 14.md)
    * [Commitment 15](/en/commitment 15.md)
    * [Commitment 16](/en/commitment 16.md)
    * [Commitment 17](/en/commitment 17.md)
    * [Commitment 18](/en/commitment 18.md)
    * [Commitment 19](/en/commitment 19.md)
    * [Commitment 20](/en/commitment 20.md)
    * [Commitment 21](/en/commitment 21.md)
    * [Commitment 22](/en/commitment 22.md)
    * [Commitment 23](/en/commitment 23.md)
    * [Commitment 24](/en/commitment 24.md)
    * [Commitment 25](/en/commitment 25.md)
    * [Commitment 26](/en/commitment 26.md)

* [Peer exchange and learning](/en/peer-exchange-learning.md)

* [Conclusion: lessons learned and next steps](/en/conclusion.md)

* [Contribute](/en/contribute.md)

* [Annexes](/en/annexes.md)

  * [Ministries and government departments involved in the 2015-2017 National Action Plan](/en/administration-list.md)
  * [Organizations involved in the elaboration of the 2015-2017 National Action Plan](/en/organizations-list)

  * [Organize an “Open Ministry” event on your commitments!](/en/open-ministry.md)



