# 5. Peer exchange and learning

The [Open Government Partnership](http://www.opengovpartnership.org/)is an experience-sharing platform for the world-wide community of state reformers. This network enables member countries to exchange good practice and resources to support openness and transparency efforts.

France is carrying forward several strong initiatives for peer exchanges and international cooperation, including on a technical level. Opening up and reusing open government methods and tools are among the principles guiding France’s action. The aim is to build and sustain common open government resources.

## **Sharing and reuse of the data.gouv.fr platform worldwide**

Since 2015, technical collaboration has been established with various countries around **udata**, the [data.gouv.fr](https://www.data.gouv.fr/fr/) platform engine, which Etalab has openly developed since 2013 on Github. Etalab has thus created a community of contributors to udata through [a discussion and mutual aid service](https://gitter.im/etalab/udata).

**Current technical collaboration:**

* **With Luxembourg**: **:

Since early 2016, the [data.gouv.fr](https://www.data.gouv.fr/fr/) teams have been assisting Luxembourg developers to take ownership of the code.In April 2016, Luxembourg officially launched the [English](https://data.public.lu/en/) and [French](https://data.public.lu/fr/) versions of its open data interoperability platform at its “[Game of Code](http://www.gameofcode.eu/)“ event. Luxembourg’s goal is to offer a tool which enables development of services and creates value through reuse.

[This cooperation has enabled the data.gouv.fr teams to improve thedata.gouv.fr codeanditsdocumentation. Since then a total revamp of the](https://udata.readthedocs.io/en/latest/) [data.gouv.fr](https://www.data.gouv.fr/fr/) [site registration process has begun](https://udata.readthedocs.io/en/latest/).

[**For more information**](https://www.etalab.gouv.fr/lancement-de-la-plateforme-udata-du-luxembourg)

![](/assets/import.png)

* **With Serbia:** Two Serbian Government developers are working on adopting udata for Serbia’s open data portal.**: Two Serbian Government developers are working on adopting udata for Serbia’s open data portal. **Two Serbian Government developers are working on adopting udata for Serbia’s open data portal.

* **With Togo:**Togo has launched its project to create the opendata.tg portal based on udata. A team of two developers is in charge of the project and compares notes with [data.gouv.fr](https://www.data.gouv.fr/fr/) French developers and [data.public.lu](https://data.public.lu/fr/) Luxembourg developers on Git. In addition to adopting the portal, the Etalab mission has also talked to Togo about its experience in collecting data from government departments. **Togo has launched its project to create the opendata.tg portal based on udata. A team of two developers is in charge of the project and compares notes with [data.gouv.fr](https://www.data.gouv.fr/fr/) French developers and [data.public.lu](https://data.public.lu/fr/) Luxembourg developers on Git. In addition to adopting the portal, the Etalab mission has also talked to Togo about its experience in collecting data from government departments.

## **Exchanges and projects relating to Francophone countries’ data: \#HackFrancophonie**

On 19 and 20 February 2016, around ten Francophone countries met in Paris for a contributory event organized by the [Open Government Partnership, Etalab,Burkina Open Data Initiative, theWorld BankandCFI, the French operator in media cooperation.](http://donnees.banquemondiale.org/)

* The aim was to speed up initiatives and projects for reusing development data
* Prior to the event, the participants and Etalab teams had indexed the open data in an [open directory](https://github.com/etalab/HackFrancophonie/wiki)
* The two-day meeting between government open data and development experts and many Francophone civil society organizations \([jokkolabs](http://jokkolabs.net/), [Africtivistes](http://www.africtivistes.org/), [Social Justice](http://www.socialjustice-ci.net/), [Balai citoyen](https://fr-fr.facebook.com/CitoyenBalayeur)\) provided the opportunity for exchanging good practice on open data \(how to develop a portal, organize a reusers’ ecosystem, etc.\). Two open data officers gave presentations about what they do and what they have achieved in their ministries. An [Open Government Partnership](http://www.opengovpartnership.org/) sessionalso saw a presentation of the initiative and its successes.
* Nine concrete projects have been developed on the challenges of mapping, education, economics and democracy.

**Participating countries and data sets : **[Benin](https://github.com/etalab/HackFrancophonie/wiki/B%C3%A9nin),[Burkina Faso](https://github.com/etalab/HackFrancophonie/wiki/Burkina-Faso), [Côte d’Ivoire](https://github.com/etalab/HackFrancophonie/wiki/C%C3%B4te-d%27Ivoire),[Haiti](https://github.com/etalab/HackFrancophonie/wiki/Ha%C3%AFti), Mauritius\) and [Senegal](https://github.com/etalab/HackFrancophonie/wiki/S%C3%A9n%C3%A9gal).

![](/fr/images/hackfrancophonie.jpg)

**For more information:**

* [HackFrancophonie](https://github.com/etalab/HackFrancophonie/wiki) directory
* Day 1 summary –[Workshops on opening data between Francophone governments](https://www.etalab.gouv.fr/hackfrancophonie-jour-1-ateliers-sur-louverture-des-donnees-entre-gouvernements-francophones)
* Day 2 summary -  [Open Data Camp on open data in francophone countries](https://www.etalab.gouv.fr/hackfrancophonie-jour-2-open-data-camp-autour-des-donnees-ouvertes-dans-la-francophonie)
* [General presentation of the event](https://www.etalab.gouv.fr/hackfrancophonie-un-open-data-camp-autour-des-donnees-ouvertes-par-les-pays-francophones)

## 

## **Etalab task force welcomes foreign and regional delegations**

The Etalab task force regularly welcomes to its premises foreign and regional delegations who are passing through the French capital.

* In January 2016, Etalab welcomed a **delegation from Morocco’s central authority for the prevention of corruption \(Instance centrale de prévention de la corruption \[ICPC\]\)**, which is a member of the open government steering committee in Morocco. The discussions focused inter alia on using digital tools to improve communication with the public and engage NGOs, and on the national integrity portal developed by the ICPC.

* In April 2016, Etalab welcomed a **Portuguese delegation** to discuss opening public data, open government and data science methods for data-driven government policy.

* In early June 2016, Etalab is hosting **representatives of New Caledonia** interested in opening public data. This meeting testifies to regional actors’ growing interest in open data and open government.

* In late June 2016, Etalab is meeting an **Indian delegation** following an agreement on government reform between India’s Ministry of Personnel, Public Grievances and Pensions and France’s Ministry for the Civil Service.

## **Training the new open data generation: Open Data Youth Camp in Croatia**

## ![](/fr/images/YouthODC.png)

From 29 August to 2 September2015, 21 young people from Croatia, Serbia, France and the United Kingdom learned about the principles and stakes involved in opening data during an [Open Data YouthCamp](http://odbc.codeforcroatia.org/) in Rovinj, in Croatia. [Thanks to the Croatian Government, French Embassy and Etalab task force, three young participants and an expertwere selected to represent France at the event. After several days](https://www.etalab.gouv.fr/les-jeunes-et-lopen-data)’ training, sharing of personal experience and practical workshops,participants drafted aYouth Open Data Declaration which was presented at the 2015 Open Government Partnership World Summit in Mexico.

[For more information](https://www.etalab.gouv.fr/la-nouvelle-generation-de-lopen-data-retours-dexperience-de-lopen-data-youth-camp)

## **Cooperation with the United Kingdom on the data economy**

* On 27 July, George Osborne, then United Kingdom Chancellor of the Exchequer, and Emmanuel Macron, French Minister of the Economy, Industry and the Digital Sector announced the establishment of a Franco-British taskforce on the data economy. The United Kingdom and France decided to pool their efforts to understand how best to exploit the data growth potential. The working group is chaired by Sir Nigel Shadbolt, Chairman and Co-Founder of the Open Data Institute, and Henri Verdier, Chief Data Officer and Head of the Interministerial Directorate for the Digital Sector and Information and Communications System \(Direction interministérielle du numérique et du système d’information et de communication– DINSIC\) in the Secretariat-General for Government Modernization \(SGMAP\). It has the following objectives:

  * to strengthen collaboration between British and French public bodies on open data, and improve public services through data analytics;
  * encourage businesses \(especially SMEs\) to make good use of public and private data in order to increase their competitiveness;
  * promote a data culture and data science skills in the public and private sectors; and
  * create the conditions for developing a data economy ecosystem, inter alia by enhancing citizen and business confidence.

* After several working meetings, a final report will identify opportunities for concrete collaboration between the two countries focusing on exploitation of the data revolution by businesses, particularly start-ups and SMEs, and modernizing government by developing data-based public services.

For more information [in French](https://www.etalab.gouv.fr/lavancement-de-la-taskforce-franco-britannique-sur-leconomie-de-la-donnee) or [in English](https://www.gov.uk/government/news/new-boost-for-digital-economy).

## **Participation in the Global Partnership for Sustainable Development Data**

* On 25 September 2015, UN Member States adopted 17 [Sustainable Development Goals](http://www.undp.org/content/undp/fr/home/mdgoverview/post-2015-development-agenda.html) \(SDGs\) to be achieved by 2030. Data use and dissemination of open government principles are essential for measuring progress in development. To build on these, a ministerial-level meeting of the Open Government Partnership steering committee and launch of the [Global Partnership for sustainable development data were organized in the margins of the United Nations General Assembly.](http://www.data4sdgs.org/) Annick Girardin, then Minister of State for Development and Francophony, spoke for France at the two events. In her [speech](https://www.etalab.gouv.fr/wp-content/uploads/2015/10/Lundi-28-septembre-partenariat-mondiale-donn%C3%A9es-de-d%C3%A9veloppement.pdf) she stressed France’s two commitments: to promote the issue of development data during the future French OGP presidency, and to increase financial support for the Economic and Statistical Observatory for sub-Saharan Africa \(Afristat\).

[For more information](https://www.etalab.gouv.fr/mettre-les-donnees-au-service-des-objectifs-du-developpement-durable)

* On 15 December, in line with these commitments, Annick Girardin and Axelle Lemaire, Minister of State for the Digital Sector, presented their[“Development and digital technology” Government Action Plan](http://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-numerique/evenements/article/developpement-numerique-presentation-par-annick-girardin-et-axelle-lemaire-du). With this plan, France is determined to support developing countries’ digital efforts, which will contribute to the success of the new sustainable development agenda adopted in September 2015.

## **Participation in OECD working groups**

The Etalab task force is participating both in the working groups organized by the OECD to advise OGP member countries on drawing up their National Action Plans and in international meetings in which it is promoting open government principles.

## **Participation in Open Government Partnership working groups**

France is participating in the different Open Government Partnership working groups. In the Open Government Partnership’s “open data” working group, Etalab has:

* Sounded out the 200 participants on their understanding of “general interest data” in their countries
* Reread and commented on the“open data” commitments of the National Action Plans of Côte d'Ivoire, Paraguay, Romania and also Canada

[For more information](http://www.modernisation.gouv.fr/laction-publique-se-transforme/en-ouvrant-les-donnees-publiques/partenariat-pour-un-gouvernement-ouvert-espace-de-cooperation-internationale)

