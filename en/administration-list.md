# Ministries and government departments involved in the 2015-2017 National Action Plan

* Prime Minister

  * Ministry of State for State Reform and Simplification

  * Directorate of Legal and Administrative Information

  * National Agency for IT System Security

* Ministry of Foreign Affairs and International Development.

* Ministry of the Environment, Energy and Marine Affairs

* Ministry of National Education, Higher Education and Research

* Ministry of Justice

* Ministry of Town and Country Planning, Rural Affairs and Local Government

* Ministry of the Interior

* Ministry of Finance and Public Accounts

* Ministry of the Economy, Industry and the Digital Sector

* Ministry of State responsible for the Digital Sector

* Minister of Urban Affairs, Youth and Sport

* Ministry for the Civil Service

### **Other lead institutions**

* Cour des comptes

* High Authority for Transparency in Public Life



