# Organize an “Open Ministry” event on your commitments!

Lead ministries can also hold their own“Open Ministry” contributory event to follow up the implementation of France’s National Action Plan, with the support of the Etalab Task Force of theSecretariat-General for Government Modernization.



### **What is an“Open Ministry” event?**

To assess the implementation of France National Action Plan, an online consultation mechanism was established, using this e-book, a physical book or via the “Open Ministry events”.

The “Open Ministry” is a  new format for dialogue and co-construction of government policy, bringing civil society and the government together in contributory workshops in lead ministries.

A first “Open Ministry” event took place on 21 June 2016 at the Ministry of State for State Reform and Simplification, which is leading ten commitments.From 5 p.m. to 8 p.m. participants were divided into workshops in which they talked to government officials before presenting their contributions to the Minister of State.

The current “Open Ministry” kit has been developed from the positive experience of this first event, so that all ministries wishing to do so may take up this initiative.

[See the “Open Ministry” programme at the Ministry of State for State Reform and Simplification](https://www.etalab.gouv.fr/wp-content/uploads/2016/06/20160621_Programme-Ministere-ouvert-1-VF.pdf).

“Open Ministry” events may be held throughout the year until submission of the final self-assessment report in July 2017.



### **How do you organize an“Open Ministry” event?**

If you wish to organize an “Open Ministry”, email us at [gouvernement-ouvert@etalab.gouv.fr ](/mailto:gouvernement-ouvert@etalab.gouv.fr)\(or write to your usual contact in the Etalab task force\). We will then be able to discuss your project in greater detail.



On this page, we are making available all the methodological and visual resources you will need to set up your “Open Ministry” event.

* **Typical workshop format type**&gt;[Open Ministry - format type workshop](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Minist%C3%A8re-ouvert-format-type-atelier.odt)

* **Model for describing the event programme**&gt;[Open Ministry - Model Programme](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Minist%C3%A8re-ouvert-Mod%C3%A8le-programme.odt)

* **Presentation support used in plenary session**&gt;[Open Ministry – Plenary model presentation](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Ministere-ouvert-Mod%C3%A8le-pr%C3%A9sentation-pl%C3%A9ni%C3%A8re.odp)

* **Workshop support**&gt;[Open Ministry - Support workshops](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Minist%C3%A8re-ouvert-Mod%C3%A8le-supports-ateliers.odp)

* **“Open Ministry” logo**&gt; [download here](https://www.etalab.gouv.fr/wp-content/uploads/2016/07/Logo_Ministere_Ouvert_V17062016.png)

### **How do you promote your“Open Ministry” in the context of the monitoring of the National Action Plan?**

Etalab will relay your “Open ministry” event through its communication channels and can also mobilize its ecosystems . Feedback from workshops can be directly integrated as contributions to the follow-up/monitoring of the action plan, on the model of the [Open Ministry feedback \#1](https://forum.etalab.gouv.fr/users/MinistereOuvert1).

Contact:[open-government @etalab.gouv.fr](mailto:gouvernement-ouvert@etalab.gouv.fr)



