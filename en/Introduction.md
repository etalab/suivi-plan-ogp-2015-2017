# 1. Introduction and background

## **France National Action Plan**

[France National Action Plan 2015-2017 “For a transparent and collaborative government](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/)“, published on 16 July 2015 contains 26 commitments structured around 4 priority areas:

* Ensure accountability
* Consult, debate and co-create public policy
* Share digital resources useful for economic and social innovation
* Open up public administration and government services

The last part of the action plan contains 3 commitments applying open government principles to climate action and sustainable development. They were implemented in the context of [COP21](http://www.cop21.gouv.fr/), held in Paris in December2015.

## **National Assembly Action Plan**

Concurrently, and in accordance with the constitutional principle of the separation of powers, the National Assembly has joined in the open government initiative by publishing its own action plan “[Towards a twenty-first century National Assembly](http://www.opengovpartnership.org/sites/default/files/Plan%20d%E2%80%99action%20de%20l%E2%80%99Assembl%C3%A9e%20Nationale.pdf)”. Its assessment is also an independent process.

### **The National Action Plan’s 26 commitments fully reflect the four key principles of open government:**

* First there must be more **transparency **at every level of government action and economic life. Major examples of this are the data relating to local government financial activity \([commitment 1\)](/en/commitment 19.md), international development aid \([commitment 3](/en/commitment 3.md)\), public officials assets \([commitment 6](/en/commitment 6.md)\), climate and sustainable development \([commitment 25](/en/commitment 25.md)\).

* Government decision-making must be opened up to **civic participation** by joint preparation of legislation and civil society’s involvement in public life \(commitments [10](/en/commitment 10.md), [11](/en/commitment 11.md), [12](/en/commitment 12.md), [13](/en/commitment 13.md), [19](/en/commitment 19.md), [24](/en/commitment 24.md), [26](/en/commitment 26.md)\), inviting public assessments of government policies \([commitment 4](/en/commitment 4.md)\), open justice and better understanding on how institutions work \(commitments [14](/en/commitment 14.md) and [5](/en/commitment 5.md)\). Government services should refresh and open up to more diverse backgrounds and new practices \(commitments [20](/en/commitment 20.md), [21](/en/commitment 21.md), [22](/en/commitment 22.md), [23](/en/commitment 23.md)\).

* France must play the greatest possible role in international initiatives linked to public procurement transparency \([commitment 2](/en/commitment 2.md)\), beneficial owners of companies \([commitment 7](/en/commiment 7.md)\), payments and revenues from the extractive industries \([commitment 8](/en/commitment 8.md)\), trade negotiations \([commitment 9](/en/commitment 9.md), [commitment 16](/en/commitment 16.md)\) – all contributing to achieve **greater accountability of government action**.

* **Digitization **and **innovation** are at the heart of the open government initiative, thanks to a strong open data policy \([commitment 15](/en/commitment 15.md)\), and amplified actions on government digital resources sharing, which transforms the way we conceive public services \(commitments [17](/en/commitment 17.md) and [18](/en/commitment 18.md)\).

### **This mid-term self-assessment report marks the first year of France National Action Plan implementation:**

It details the method of [drawing up, implementing and monitoring the plan](/en/consultation.md), and [sets out the progress achieved to date](/en/summary-table.md) in implementing the commitments, as well as [actions taken in order to share France’s experience and resources](/en/peer-exchange-learning.md) with other countries. It finally sets [the next steps](/en/conclusion.md).

It will be continuously expanded until July 2017 when France is due to submit a final self-assessment report and its second action plan.

A“[Contribute](/en/contribute.md)” section is included where you can make suggestions on improving the action plan’s method and monitoring tools. As a result of the first consultation period [open from 13 to 30 June 2016](https://www.etalab.gouv.fr/pgo-consultation-sur-le-rapport-detape-du-plan-daction-national-2015-2017), the initial version of the one-year mid-term report has been expanded.

**Global timetable for France in the Open Government Partnership: action plan implementation and report**

![](/fr/images/ogp-timeline-france.PNG)

[For more information see France section on the OGP site](http://www.opengovpartnership.org/country/france/)

# 



