# Organizations involved in the elaboration of the 2015-2017 National Action Plan

* Action Contre la Faim, France \(Action against Hunger, France\)

* Altercarto \(association pooling and making available public localized information\)

* Assemblée virtuelle \(Virtual Assembly – encourages development of collaboration in the world of transition\)
* Centre de Recherches Interdisciplinaires \(Interdisciplinary Research Centre\)
* Change.org
* Club Jade \(a think tank promoting democracy, innovation and Europe, with a focus on digital technology as a major contributor to economic and social development\)
* Cobble Camp
* Comité 21 \(main French network of actors for sustainable development\)
* Commission nationale du débat public – CNDP \(National Commission for Public Debate\)
* Conseil national du numérique \(National Council for the Digital Sector\)
* Décider Ensemble \(Decide Together\) \[Think tank set up in 2005 at the request of the Minister for Ecology and Sustainable Development, brings together people from different strands of French society to create and promote a culture of shared decision-making\]
* Démocratie Ouverte \(Open Democracy\)
* Empreintes citoyennes \(Citizens’ fingerprints – association encouraging active citizenship\)
* Five by Five \(a kind of innovation agency and the Open Data Institute’s outpost in Paris\)
* Fondation internet nouvelle génération – FING \(FING creates, detects and shares novel and actionable ideas to anticipate digital transformations\)
* Nicolas Hulot Foundation for Nature and Mankind – leading environmental NGO aiming at encouraging changing consumption patterns throughout society
* GIS démocratie et participation \(National research group on participatory democracy and public participation in decision-making\).
* HelloAsso \(free- to-use platform for online collection\)
* Institut de la Concertation \(French network which brings together actors and professionals working for civic participation\)
* La quadrature du net \(non-profit association defending rights and freedom of citizens on the Internet\)
* La mêlée numérique \(Information and communication technologies forum and expo\)
* Linagora \(leading French free sofltware company providing innovative solutions for enterprise collaboration\)
* Mouvement Colibris \(movement for the earth and humanism\)
* ONE, France \(an international organization campaigning against extreme poverty and avoidable diseases particularly in Africa\)
* Open Knowledge, France \(created in 2013 with the aim of promoting access, publication, use and reuse of public interest knowledge and commons\)
* OpenStreetMap France \(French NGO committed to furthering use of geographic information tools to improve data gathering and analysis for emergency relief and development programmes around the world\)
* Outils réseau \(initiates and supports cooperative practices, using Internet tools\)
* Oxfam France
* Parlement & Citoyens \(a web platform allowing members of French Parliament to involve citizens in drafting their bills\)
* Publish What you Fund
* Publish What you Pay
* Regards citoyens \(enables Parliament and French citizens to seek together solutions to France’s problems\)
* Réseau d’experts Etalab \(Etalab experts network\)
* SavoirCom1
* Secours catholique \(a service of the Catholic Church and member of the Caritas Internationalis confederation\)
* Talking things
* Transparency International France
* Université Technologie de Compiègne \(Compiègne Technological University\)



