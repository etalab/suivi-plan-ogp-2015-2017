	

# 2.2. Consultation during the National Action Plan's implementation

### Concrete implementation of the National Action Plan requires providing citizens with the tools to monitor and participate online and offline.

### **Several mechanisms are proposed:**

* Monitor the progress of the actions undertaken, commitment bycommitment, published in this report. Citizens can comment directly on the implementation, through a forum integrated within every follow-up page.

* Send a written contribution to[gouvernement-ouvert@etalab.gouv.fr](mailto:gouvernement-ouvert@etalab.gouv.fr). Contributions are made public.

* Suggest improvements in the National Action Plan consultation and monitoring methodology, in this book’s[**“Contribute”**](#_bookmark43)section:contributors may suggest consultation tools and methodologies or any other idea to improve the monitoring of the commitments \(construction of quantitative and qualitative indicators, face-to-face workshop formats, etc.\)



Concurrently, government departments are invited to report on the progress of their actions.



### **Timetable**:

### ![](/assets/2016 06 13_Calendrier suivi PAN EN.png)



