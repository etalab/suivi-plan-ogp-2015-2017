# 6. Conclusion and next steps

## **The open government process France initiated after joining the Open Government Partnership in 2014 is moving institutions and government departments and agencies towards more transparent and concerted public action**

### **Several lessons are clear from the action plan’s first year of implementation:**

* **Upstream involvement of a significant number of ministries in the National Action Plan is a key factor for its success**

  * It has persuaded all government departments to follow the same policy and speed up numerous projects.
  * Government-civil society alliances have already spawned concrete projects such as the Open Law initiative based on opening up the law.

* **Numerous commitments have been enshrined in law**: bills for legislation on public economic life, modernizing the civil service, regional reform, the digital economy, biodiversity, etc. The legislative framework for opening up public action is burgeoning: the creation of a public service focused on data, the creation of public directories on lobbying transparency, tightening the rules of conduct for public officials and fighting corruption are the major components of open government and are now enshrined in law or subject to parliamentary debate.

* **France is increasing its presence in international cooperation forums or actions, **, eg the Contracting 5 group on public procurement, \([for further information](https://www.etalab.gouv.fr/sommet-anti-corruption-la-france-sengage-sur-des-registres-publics-et-standards-ouverts)\).

### **Major areas for progress have already been identified and could be integrated in future open government initiatives, including:**

* Drawing up commitments which can be more easily measured by quantitative and qualitative indicators;
* Simplifying the process of monitoring the different government departments’ implementation of the commitments and opening this to civil society ;
* Increasing the involvement of technical experts \(developers, data scientists, architects, etc.\) in implementing the action plan.

### **To give long-term impetus to French action to promote open government, several steps have been identified:**

* The “Open Ministry process \([for more information](https://www.etalab.gouv.fr/plan-daction-pgo-un-premier-evenement-ministere-ouvert-le-21-juin-2016)\): regular meetings between representatives of government departments and of civil society to monitor implementation of the commitments. An first meeting was scheduled for 21 June 2016 at the Ministry of State for State Reform and Simplification;

* A continuous consultation mechanism for both actions and method;

* Coordination of French actions with the Open Government Partnership agenda. Forthcoming meetings include:

  * At the United Nations General Assembly, in late September 2016, France will publish the English version of its mid-term assessment report.
  * The Independent Reporting Mechanism report will be published at the end of September 2016.
  * The Global OPG Summit will be held in Paris from 7 to 9 December 2016: a progress report on the action plan will be produced and the monitoring mechanisms will be integrated into the open government toolbox.
  * The second National Action Plan and final self-assessment report will be published in summer 2017
  * The Independent Reporting Mechanism report will be published at the end of September 2017.
  * Civil society and government departments will work continuously on drawing up the next action plan.



