# Mid-term self-assessment report of France’s 2015-2017 National Action Plan “For a transparent and collaborative public action”

This e-book is a digital version of the mid-term self-assessment report of France’s 2015-2017 National Action Plan, prepared under the [Open Government partnership \(OGP\)](http://www.opengovpartnership.org/). It sets out commitment by commitment, the progress of ministerial action on open government.

This e-book is open to public contributions \(integrated forum\): anyone may propose modifications, suggest other examples of successes and request new types of collaboration between civil society and the government. The forum will remain open until July 2017, when France submits a final self-assessment report.

The PDF version of this e-book is also available:

* [Initial version, published in June 2016, before consultation](https://suivi-gouvernement-ouvert.etalab.gouv.fr/fr/documentation/documentation/suivi-plan-ogp-2015-2017_fr-version-initiale.pdf) \(in French\)

* [Updated version, published in July 2016, after the consultation process](https://suivi-gouvernement-ouvert.etalab.gouv.fr/fr/documentation/documentation/suivi-plan-ogp-2015-2017_fr-version-initiale.pdf) \(in French\)

The source code for this digital book is also available on the following link: [FramaGit](https://framagit.org/etalab/suivi-plan-ogp-2015-2017/).

For any question or suggestion: [gouvernement-ouvert@etalab.gouv.fr](mailto:gouvernement-ouvert@etalab.gouv.fr)

