# 4.2. Details of the commitments and results obtained

### **Ensure accountability**

### Improve transparency in public expenditure and accounts

* [Commitment 1: Enable citizens to consult, understand and reuse financial data and local government decisions](/en/commitment 1.md)

* [Commitment 2: Increase transparency in public procurement](/en/commitment 2.md)

* [Commitment 3: Increase transparency in international development assistance](/en/commitment 3.md)

#### 

### Release government data to allow public scrutiny

* [Commitment 4: Open access to evaluations of public policies and their conclusions](/en/commitment 4.md)

* [Commitment 5: Increase citizen involvement in the work of the Cour des comptes](/en/commitment 5.md)

#### 

### Increase transparency in public officials’ declarations of interests and assets

* [Commitment 7: Identify thebeneficial owners of legal entities registered in France to fight effectively against money laundering](/en/commiment 7.md)

* [Commitment 8: Strengthen transparency in payments and income from the extractive industries](/en/commitment 8.md)

* [Commitment 9: Increase transparency in international trade negotiations](/en/commitment 9.md)

#### 

# **Consult, discuss and co-produce**

### **Promote contributory and collaborative public action**

* [Commitment 10: Provide citizens with new means to participate in public life and involve them in problem solving process](/en/commitment 10.md)

* [Commitment 11: Co-produce with civil society the data infrastructure essential to civil society and the economy](/en/commitment 11.md)

* [Commitment 12: Further expand the opening of legal resources and collaboration with civil society on drawing up legislation](/en/commitment 12.md)

#### 

### **Reform consultation and cooperation practices**

* [Commitment 13: Capitalize on previous consultations and remodel citizens' participatory mechanisms](/en/commitment 13.md)

* [Commitment 14: Strengthen mediation and citizens’ ability to act in judicial matters](/en/commitment 14.md)

#### 

### **Share digital resources**

* [Commitment 15: Strengthen policy on data opening and circulation](/en/commitment 15.md)

* [Commitment 16: Promote the opening of government calculation models and simulators](/en/commitment 16.md)

* [Commitment 17: Transform the government’s technological resources into an open platform](/en/commitment 17.md)

* [Commitment 18: Strengthen interaction with users and improve public services through e-government](/en/commitment 18.md)

#### 

# **Continue to open up government**

### **Encourage public engagement in support of the public authorities**

* [Commitment 19: Enable the involvement of civil society organisations to support schools](/en/commitment 19.md)

### **Open up access to the civil service**

* [Commitment 20: Diversify recruitment within public institutions](/en/commitment 20.md)

### **Promote a culture of government openness and innovation**

* [Commitment 21: Promote openness, a data-driven culture and digital literacy](/en/commitment 21.md)

* [Commitment 22: Promote innovation and develop research on open government](/en/commitment 22.md)

### **Strengthen the Civil Service Code of Conduct**

* [Commitment 23: Make public officials aware of their responsibility for preventing conflicts of interest](/en/commitment 23.md)

# **Open government: a tool to address climate and sustainable development issues**

* [Commitment 24: Involve civil society in the COP21 conference and promote transparency on the agenda and in negotiations](/en/commitment 24.md)

* [Commitment 25: Make available climate and sustainable development data and models](/en/commitment 25.md)

* [Commitment 26: Forge new collaborations with civil society to develop innovative solutions to address the climate and sustainable development challenges](/en/commitment 26.md)



