# 2. Process of developing and implementing the National Action Plan

This section sets out the mechanism for consultation put in place when the National Action Plan was drafted, implemented and the self-assessment report drawn up.

* [2.1. Consultation on the elaboration of the National Action Plan  ](/en/consultation-drafting.md)

* [2.2. Consultation on the implementation of the National Action Plan  ](/en/consultation-implementing.md)

* [2.3. Consultation on the self-assessment report](/en/consultation-report.md)



