## 2.1. Consultation on drafting the National Action Plan

### The elaboration of the Action Plan was coordinated by the [Etalab task force](/etalab.gouv.fr), within the Secretariat-General for Government Modernization \(SGMAP\), between October 2014 and June 2015, through several consultation channels:

* An on-line consultation forum, organized by the [National Digital Counci](http://www.cnnumerique.fr/)l, from the 3rd of November 2014 to the 28th of February 2015:

  * [The consultation discussions and summary of the contributions](https://contribuez.cnnumerique.fr/debat/open-gov-comment-faire-progresser-la-transparence-de-l%E2%80%99action-publique-et-la-participation) are available online. On the theme of transformation of government action, **two consultations focused on “**[**Open Data**](http://contribuez.cnnumerique.fr/debat/open-data-une-d%C3%A9mocratie-plus-ouverte-et-de-nouveaux-biens-communs)“ and**“**[**Open Government”**](http://contribuez.cnnumerique.fr/debat/open-gov-comment-faire-progresser-la-transparence-de-l%E2%80%99action-publique-et-la-participation). Contributions published on other topics of consultation \(e.g.: [State technological strategy and public services](http://contribuez.cnnumerique.fr/debat/strat%C3%A9gie-technologique-de-letat-et-services-publics)\) also led to further discussion on open government.

  * The consultation data were published in [open data on data.gouv.fr](https://www.data.gouv.fr/fr/datasets/concertation-nationale-sur-le-numerique-2/). The proposals which have emerged through these consultations contributed to the definition of the commitments, some of which are included as verbatim in the National Action Plan.

* Workshops organized by organizations and stakeholders, including [La Cantine brestoise](http://www.lacantine-brest.net/) and [Décider ensemble](http://www.deciderensemble.com/page/9519-)

* ·Bilateral meetings and working sessions with civil society organizations and expert groups \([see list of the organizations](/en/organizations-list.md)\)

* Open meetings, workshops and contributory events and organized throughout France \(see table below\)

* Mobilization of [Etabab’s experts network](https://www.etalab.gouv.fr/reseau-dexperts), composed of qualified individuals from civil society \(citizen groups, research and business associations, open data\)

* Proposals formulated in numerous public reports from research institutes and think tanks
* Presentations and progress reports made with public stakeholders and civil society \(links to progress report presentations below\)

Thanks to these interactive meetings, many stakeholders have been aware of the importance of the objectives and principles of open government. They made valuable contributions to identifying ways to get the effort off the ground. The mobilization of many ministries in the initiative has also brought strong ownership of the commitments, thereby speeding up progress on the open government agenda. [See the list of ministries and government departments involved in the Action Plan](/en/administration-list.md).

### **Contributory events**

| Events | Date and location | Theme | Organization |
| :---: | :---: | :---: | :---: |
| Open World Forum | 31/10/2014 in Paris | [From Open Data to Open Gov: the role the civil ssociety](http://www.openworldforum.paris/fr/tracks/from-open-data-to-open-gov-the-role-of-the-civil-society-workshop-) | Facilitation by Etalab |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |

### **Presentations of mid-term reports**

* [Tuesday, 16 December 2014 in Numa](http://fr.slideshare.net/Etalab/presentation-point-etape-1-ogp)

* [Tuesday, 17 February 2015 at Superpublic](http://fr.slideshare.net/Etalab/support-point-dtape-pan-daction-national-ogp)

* [Tuesday, 17 March 2015 in Numa](http://fr.slideshare.net/Etalab/20150317-ogp-point-dtape)



