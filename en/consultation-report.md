# 2.3. Consultation on the self assessment report

### **The one-year self-assessment report was submitted for **[**public consultation in June 2016, from the13th to the 30th.**](https://www.etalab.gouv.fr/pgo-consultation-sur-le-rapport-detape-du-plan-daction-national-2015-2017)

### This consultation was carried out online and offline.

* Online through the open discussion forum

* During an [Open Ministry, organized on the 21st of June, 2016](https://www.etalab.gouv.fr/plan-daction-pgo-un-premier-evenement-ministere-ouvert-le-21-juin-2016), at the Ministry of State for State Reform and Simplification. Participants from civil society organizations and public administrations co-evaluated 10 commitments’ state of implementation.

### **A process of dialogue and continual improvement**

In order to maintain the consultative approach set up since the elaboration of the National Action Plan, the discussion forum remains open to contributions. [“Open Ministry” meetings can](/en/open-ministry.md) be organized in the lead ministries in order to pursue this dialogue between government departments and civil society on the 2015-2017 Action Plan commitments, and on future commitments to be added to the next Action Plan.

