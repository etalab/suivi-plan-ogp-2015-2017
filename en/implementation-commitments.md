# 

# 4. Implementation of the National Action Plan commitments

This section [summarizes](/en/summary-table.md) and [details](/en/details-commitments.md) the progress made in the implementation of the National Action Plan commitments.

### **Summary:**

[A table summarizes](/en/summary-table.md) the global implementation of the commitments. For each commitment, it gives its status \(not started, partial, substantial, and complete\) and the progress made in relation to the set timetable.

### **Detailed section:**

* gives details of the lead institutions, stakes and roadmap,

* followed first by a table setting out the progress in each action’s implementation on the roadmap using several tabs: what has been implemented and what are the next steps. Each action is assigned a status: **not started, partial, substantial or complete**.

* and then completed by one or more concrete examples of the commitment’s success.

The Etalab task force realized this monitoring work, in coordination with all the commitments’ lead institutions and civil society organisations. Since the Action Plan was adopted in July 2015, Etalab has been holding discussions with a network of open data officers from other government departments and supporting their efforts to open data, increase transparency and forge new collaborations. This follow-up is then shared at meetings bringing the different ministries together to define the next measures to implement each action.

**A first meeting with the **[**relevant government departments**](/en/administration-list.md)** took place on 13 May 2016.**

