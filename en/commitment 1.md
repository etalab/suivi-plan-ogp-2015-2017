# [Commitment 1: Enable citizens to consult, understand and reuse financial data and local government decisions](undefined)

### **Lead institutions:**

* Ministry of the Interior

* Ministry of Finance and Public Accounts

* Ministry of Town and Country Planning, Rural Affairs and Local Government

* Cour des comptes \(Audit Court\)
* Ministry of the Environment, Energy and Marine Affairs

### **Stakes:**

To better satisfy citizens’ legitimate expectations and in view of the large proportion of public funds spent by local government, their financial transparency should be ensured.

**Description of the commitment:**

* **Publish local government data in open format**

  * Enable citizens to have a better understanding of the financial processes within local and regional authorities
  * Regularly make financial court data available
  * Strengthen local government open data: enshrine in law the requirement for local authorities with over 3,500 inhabitants \(including communes and public inter-municipal cooperation establishments \[EPCIs\]\) to release public data.

* **Publish the decisions and reports of municipal council meetings online**

  * Publish in electronic format and provide permanent access free of charge, in addition to a paper version, to local authority collections of administrative acts, deliberations and municipal by-laws
  * Post online on the commune’s website, where it exists, municipal council meeting minutes within a week of the meeting and leave them posted for at least six years

* **Publish in open format data relating to building permits: **Establish a working group with stakeholders to work on the progressive release in open format of building permit data by 2017

[For more information](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-depense-et-comptes-publics/engagement-1.html)

### Description of Results:

| Actions | Results | Next steps | Status |
| :--- | :--- | :--- | :--- |
| Publish in open data the general operating grant \(dotation globale de fonctionnement \[DGF\], the central government financial contribution to regional and local authorities\) | A research tool which makes it possible to extract [data on grants](http://www.dotations-dgcl.interieur.gouv.fr/) is available on the Interior Ministry’s website. The physical and financial criteria used as the basis for calculating the general operation grant are downloadable via [different searches](http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition.php) on the website of the Local Government Directorate-General \(DGCL\). [This tool has been listed on data.gouv.fr](https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-dotations-aux-collectivites-territoriales-et-a-leurs-criteres-de-repartition-referencement/). | Put direct links to data sets on data.gouv.fr. | Substantial |
| Publish in open data on data.gouv.fr all local account balances of local authorities and groups with specific taxation from the 2013 financial year. | [Account balances](https://www.data.gouv.fr/fr/datasets/?q=balances+comptables&organization=534fff8ea3a7292c64a77f02) are available from 2013 on data.gouv.fr. | Obtain historical account balances. Update data annually. | Complete |
| Make it compulsory for local executives and EPCI chairmen to present a report to the deliberative assembly \(municipal, department or regional council\) on any action taken in the wake of observations of the chambre régionale des comptes \(CRC\). | This commitment was included in article 107 of the Act on a New Territorial Organization for the Republic, popularly called [loi NOTRe](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030985460&categorieLien=id) \(new article L2143-7 of the Financial Jurisdictions Code\). It contains two types of provisions related to action taken by local authorities in the wake of the CRC’s observation reports and transmission of these reports by EPCI chairmen to member commune mayors. The aim of these provisions is to strengthen the effectiveness and usefulness of CRC monitoring of local government management. They require the executive to communicate on the action taken to follow up CRC observations and recommendations in a report first presented to their deliberative assembly \(within one year\) and then forwarded to the CRC. [A \#DataSession](https://www.ccomptes.fr/Actualites/A-la-une/Premiere-DataSession-a-la-Cour-des-comptes), held at the Cour des comptes, led to the idea of a tool for monitoring application of the CRC report recommendations \([see “reco-tracker” tool](https://forum.etalab.gouv.fr/t/projet-suivi-des-recommandations-et-controle-citoyen/1237)\). | Monitor the measure’s application \(CRC summaries\). [The timetable for the implementation of the loi NOTRe is available on Légifrance.](vhttps://www.legifrance.gouv.fr/affichLoiPubliee.do?idDocument=JORFDOLE000029101338&type=echeancier&typeLoi=&legislature=14) | Substantial |
| Regularly make available financial court financial data: data underpinning discussions of local finances and certain data of financial court activity \(including the updating of the Cour des conptes’ publications list and financial courts’ resources\). | Cour des comptes r[eport data](https://www.data.gouv.fr/fr/organizations/cour-des-comptes/#datasets) on local government finances is available on data.gouv.fr. New data sets were published on 27 May 2016 at a Cour des comptes [\#DataSession](https://www.etalab.gouv.fr/datasession-a-la-cour-des-comptes-une-premiere-brique-vers-louverture-des-decisions-de-justice): anonymized judgments of the Cour des comptes and Chambres regionals et territoriales des comptes \(2016\), and reports on these chambers’ final observations \(2013, 2014 and 2015\). See all [Cour des comptes data](/data.ccomptes.fr). | Publish data on the activities of the financial courts, including the updating of the Cour des comptes’ publications list and financial courts’ resources. Provide more published historical data sets. | Complete |
| Increase local government open data: enshrine in law the requirement for local authorities with over 3,500 inhabitants \(including communes and EPICs\) to publish information in open format. | Article 4 of the Digital Republic Bill requires local authorities with over 3,500 inhabitants to comply with government agency open data common law \(NB: this article abrogates the provisions initially included in article 106 of the loi NOTRe\). There are several current initiatives to help these local authorities in the transition towards open data, including the [OpenDataFrance](http://www.opendatafrance.net/2016/07/01/opendata-france-partenaire-des-collectivites-pour-louverture-des-donnees-publiques/) process jointly commissioned for this purpose by Axelle Lemaire and Estelle Grelier. Etalab is also working on supporting local authority efforts to make their data available: details of possible integrations may be found [here](https://gist.github.com/davidbgk/40bd03b77e7e5c8d3ae891b6cc2ffbc9). | Monitor the adoption of the Digital Republic Bill and the actions carried out with OpenDataFrance. | Substantial |
| Publish in electronic format and provide permanent access free of charge, in addition to a paper version, to local authority collections of administrative acts, deliberations and municipal by-laws. Post online on the commune’s website, when it exists, municipal council meeting minutes within a week after the meeting and leave them posted for at least six years. | The [loi NOTRe \(title IV\)](https://www.legifrance.gouv.fr/eli/loi/2015/8/7/RDFX1412429L/jo#JORFARTI000030987047) establishes this obligation \(articles 124 and 128\). [The implementing decree of article 128 was published in the Journal officiel on 12 February 2016](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032036829&categorieLien=id). |  | Substantial |
| Establish a working group going with stakeholders to work on the progressive release in open format of building permit data by 2017. | An anonymization working group was established with Etalab, the Chief Data Officer and the Office of the Commissioner General for Sustainable Development \(Commissariat Général au Développement –CGDD\). A referral to the National Data Protection and Privacy Commission \(Commission nationale de l’informatique et des libertés – CNIL\) is currently being processed. | Make the working group’s work more transparent. Monitor the CNIL’s advice and working group’s progress. | Partial |



