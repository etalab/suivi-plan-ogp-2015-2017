# 7. Contribute

In this section:

* you can use the forum to make suggestions on the methodology of drawing up and monitoring the action plan \(tools, timetable, formats, indicators, etc.\)
* written contributions sent to [gouvernement-ouvert@etalab.gouv.fr](mailto:gouvernement-ouvert@etalab.gouv.fr)will be progressively be uploaded to the forum
* information on forthcoming face-to-face events will also be published

  



