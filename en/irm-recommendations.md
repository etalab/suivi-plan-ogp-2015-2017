# 3. Recommendations of the Independent Reporting Mechanism

This section will be completed on the basis of the feedback from the [IRM - Independent Reporting Mechanism. Action plans of](http://www.opengovpartnership.org/irm/about-irm) [Open Government Partnership member states](http://www.opengovpartnership.org/)[are assessed every two years by a committee of independent experts, appointed by the Partnership](http://www.opengovpartnership.org/irm/about-irm). This Committee will produce two reports for France on the 2015-2017 National Action Plan:

* A first mid-term report, expected before the end of September 2016: in this context, on 17 June 2016 French IRM researchers launched a [consultation platform](http://participez.imodev.org/) open until 1 September2016. Dialogue events are also scheduled in Paris and elsewhere in France. Contributions will add to the independent assessment conducted by theIRM, and will concern the method of drafting, consulting on and implementing the action plan.

* A final report is scheduled for September 2017

The results of these reports will enable France to improve the implementation process of its National Action Plan.

