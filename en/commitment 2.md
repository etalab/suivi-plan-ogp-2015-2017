# [Commitment 2: Increase transparency in public procurement](undefined)

### **Lead institutions:**

* Prime Minister \(Legal and Administrative Information Directorate\)

* Ministry of Finance and Public Accounts

* Ministry of the Economy, Industry and the Digital Economy

### **Stakes:**

According to data collected by the[public procurement economic monitoring centre,](http://www.economie.gouv.fr/daj/oeap-differents-chiffrages-commande-publique)€ 71.5 billion was spent on public contracts in 2013. Transparency and proper management of public procurement are essential issues for France. They feature in the 29 January 1993 Act on the Prevention of Corruption and Transparency in Economic Life and Public Procedures \(so-called Sapin Act\) and in the Public Procurement Code \([CMP](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000005627819&idSectionTA=&dateTexte=20160331)\). From its first article, this code makes transparency one of the three fundamental principles governing public procurement.

* The advertising of public calls for tenders is covered by provisions of articles 26 and 40 of the CMP which includes the requirement to announce public calls for tenders for pre-tax amounts over € 90,000 either in the Official Bulletin of Public Contract Declarations \(BOAMP - Bulletin officiel des annonces des marchés publics\), or in a journal authorized to receive legal declarations, as well as on its buyer profile;

* Ex-post publicity of purchasers and tenderers is covered by the provisions of articles 85, 131 and 133 of the CMP

### **Description of the commitment:**

* **Standardize the format for the advertising of calls for tenders**

* **Release in open format BOAMP and publication organs data and buyer profiles**

* **Encourage development of the publication of information on awarded public contracts**

  * Make this publication mandatory for tenders above regulation thresholds and develop support measures encouraging buyers to do this for tenders below these thresholds and develop publicity for buyer profiles
  * Promote [open data](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/GLOSSARY.html#open_data), particularly by standardizing forms and presenting them in [a computer-usable](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/GLOSSARY.html#exploitable_informatiquement) and easily reusable form. Data to be opened in priority will be selected after a study phase that will not exceed one year

* **Include **[**open data clauses**](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/GLOSSARY.html#ouverture_des_donn%C3%A9es) **in contracts concluded by buyers: **Encourage service providers to open data produced during the execution of a contract by defining the relevant General Conditions of Contract \(GCC\).

[For more information](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-depense-et-comptes-publics/engagement-2.html)

### **Description of results:**

| Actions | Results | Next steps | Statut |
| :--- | :--- | :--- | :--- |
|  |  |  |  |
|  |  |  |  |

### 



